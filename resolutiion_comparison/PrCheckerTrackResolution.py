###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
 Script for the plots of tracking resolution v.s. p and eta
 The resolution is fitted by a Gaussian function

 author: Peilian Li(peilian.li@cern.ch)
 date:   01/2020
'''

import os, sys
import argparse
import ROOT
from ROOT import TMultiGraph, TMath, TAxis, TH2F, TH2, TH1, TLatex, TGaxis, TROOT, TSystem, TCanvas, TFile, TTree, TObject
from ROOT import kDashed, kOrange, kGray, kMagenta, kCyan, kRed, kGreen, kBlue, kBlack, kAzure, kTRUE, kFALSE, gPad, TH1, TH2, TF1
from ROOT import gROOT, gDirectory, gStyle, TStyle, TPaveText, TString
from ROOT import ROOT, vector, TGraphAsymmErrors

from collections import defaultdict


def get_colors():
    return [kBlack, kBlue, kRed+3, kMagenta + 2, kOrange, kCyan + 2]


def get_markers():
    return [20, 24, 21, 22, 23, 25]


def get_fillstyles():
    return [3004, 3003, 3325, 3144, 3244, 3444]


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def argument_parser():
    parser = argparse.ArgumentParser(
        description="location of the histogram file")
    parser.add_argument(
        '--filename', nargs='+', default=[], help='name of input files')
    parser.add_argument(
        '--label',
        nargs='+',
        default=["TrackRes"],
        help='name of input tuple files')
    parser.add_argument(
        '--outfile',
        type=str,
        default='TrackResolution_plots.root',
        help='name of output files')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    return parser


def PrCheckerTrackResolution(filename, label, outfile, savepdf, plotstyle):
    print("ok") 
    sys.path.append(os.path.abspath(plotstyle))
    print("ok") 
    from LHCbStyle import setLHCbStyle, set_style
    print("ok") 
    from Legend import place_legend
    print("ok") 
    setLHCbStyle()
    print("ok") 

    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()
    tf = {}
    tf = get_files(tf, filename, label)
    outputfile = TFile(outfile, "recreate")
    outputfile.cd()

    hdp_p = {}
    hdp_eta = {}
    hres_p = {}
    hres_eta = {}
    hmom = {}
    heta = {}
    print("ok") 
    canvas1 = TCanvas("res_p", "res v.s. p")
    canvas1.cd()
    for idx, lab in enumerate(label):
        hdp_p[lab] = tf[lab].Get(
            "Track/TrackResChecker/ALL/vertex/dpoverp_vs_p")
        hdp_p[lab].SetName("dpoverp_p_" + lab)
        hmom[lab] = hdp_p[lab].ProjectionX()
        hmom[lab].SetTitle("p histo. " + lab)
        hdp_p[lab].FitSlicesY()

        hres_p[lab] = gDirectory.Get("dpoverp_p_" + lab + "_2")
        hres_p[lab].GetYaxis().SetTitle("dp/p [%]")
        hres_p[lab].GetXaxis().SetTitle("p [GeV/c]")
        hres_p[lab].GetYaxis().SetRangeUser(0, 1.2)
        hres_p[lab].SetTitle("Res. " + lab)
        set_style(hres_p[lab], colors[idx], markers[idx], 0)

        if idx == 0:
            hres_p[lab].Draw("E1 p1")
            set_style(hmom[lab], kGray, markers[idx], styles[idx])
        else:
            hres_p[lab].Draw("E1 p1 same")
            set_style(hmom[lab], colors[idx] - 10, markers[idx], styles[idx])
        #hmom[lab].Scale(gPad.GetUymax() / hmom[lab].GetMaximum())
        #hmom[lab].Scale(gPad.GetUymax() / hmom[lab].GetEntries()*6)
        hmom[lab].Scale(0.00025)
        hmom[lab].Draw("hist same")

        for i in range(1, hres_p[lab].GetNbinsX()):
            hres_p[lab].SetBinContent(i, hres_p[lab].GetBinContent(i) * 100)

            print(lab + ": Track resolution (dp/p) in p region: (" +
                  format(hres_p[lab].GetBinLowEdge(i), '.2f') + ", " + format(
                      hres_p[lab].GetBinLowEdge(i) +
                      hres_p[lab].GetBinWidth(i), '.2f') + ") [GeV/c]" +
                  " --- " + format(hres_p[lab].GetBinContent(i), '.2f') + "%")
        print("-----------------------------------------------------")

    canvas1.PlaceLegend()
    canvas1.SetRightMargin(0.05)
    canvas1.Write()
    if savepdf:
        canvas1.SaveAs("trackres_p.pdf")

    print("=====================================================")

    canvas2 = TCanvas("res_eta", "res v.s. eta")
    canvas2.cd()

    for idx, lab in enumerate(label):
        hdp_eta[lab] = tf[lab].Get(
            "Track/TrackResChecker/ALL/vertex/dpoverp_vs_eta")
        hdp_eta[lab].SetName("dpoverp_eta_" + lab)
        hdp_eta[lab].FitSlicesY()
        heta[lab] = hdp_eta[lab].ProjectionX()
        heta[lab].SetTitle("#eta histo. " + lab)

        hres_eta[lab] = gDirectory.Get("dpoverp_eta_" + lab + "_2")
        hres_eta[lab].GetYaxis().SetTitle("dp/p [%]")
        hres_eta[lab].GetXaxis().SetTitle("#eta")
        hres_eta[lab].GetYaxis().SetRangeUser(0, 1.2)
        hres_eta[lab].SetTitle("Res. " + lab)
        set_style(hres_eta[lab], colors[idx], markers[idx], 0)

        if idx == 0:
            hres_eta[lab].Draw("E1 p1")
            set_style(heta[lab], kGray, markers[idx], styles[idx])
        else:
            hres_eta[lab].Draw("E1 p1 same")
            set_style(heta[lab], colors[idx] - 10, markers[idx], styles[idx])

        heta[lab].Scale(0.00045)
        heta[lab].Draw("hist same")

        for i in range(1, hres_eta[lab].GetNbinsX()):
            hres_eta[lab].SetBinContent(i,
                                        hres_eta[lab].GetBinContent(i) * 100)

            print(lab + ": Track resolution (dp/p) in eta region: (" +
                  format(hres_eta[lab].GetBinLowEdge(i), '.2f') + ", " +
                  format(
                      hres_eta[lab].GetBinLowEdge(i) +
                      hres_eta[lab].GetBinWidth(i), '.2f') + ")" + " --- " +
                  format(hres_eta[lab].GetBinContent(i), '.2f') + "%")

    canvas2.PlaceLegend()
    canvas2.SetRightMargin(0.05)
    canvas2.Write()
    if savepdf:
        canvas2.SaveAs("trackres_eta.pdf")

    outputfile.Write()
    outputfile.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    PrCheckerTrackResolution(**vars(args))
