void B_xyz(double x, double y, double z, double& B_x, double& B_y, double& B_z) {
    double dx,dy,dz,dist2,dist,ux,uy,uz,mu;
    double MY_DIPOLE = -1.6e10; 
    dx = x;
    dy = y-4000;
    dz = z-5000;
    dist2 = dx*dx+dy*dy+dz*dz; dist = sqrt(dist2);
    ux = dx/dist; uy = dy/dist; uz = dz/dist;
    mu = MY_DIPOLE*uy;
    B_x = 3*mu*ux/dist/dist2;
    B_y = (3*mu*uy-MY_DIPOLE)/dist/dist2;
    B_z = 3*mu*uz/dist/dist2;

    dy = y+4000;
    dist2 = dx*dx+dy*dy+dz*dz; dist = sqrt(dist2);
    ux = dx/dist; uy = dy/dist; uz = dz/dist;
    mu = MY_DIPOLE*uy;
    B_x += 3*mu*ux/dist/dist2;
    B_y += (3*mu*uy-MY_DIPOLE)/dist/dist2;
    B_z += 3*mu*uz/dist/dist2;
    return;                          
}

