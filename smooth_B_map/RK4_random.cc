class Field {
public:
  double Bx[81][71],By[81][71],Bz[81][71];
  Field() {};
  ~Field() {};
};

Field field[121];

void ReadField(const char *name)
{
  FILE *map;
  map = fopen(name,"r");
  int npt = 0;
  float x,y,z,bx,by,bz;
  while(fscanf(map,"%f %f %f %f %f %f",&x,&y,&z,&bx,&by,&bz)==6) {
    if(fabs(x)>4000||fabs(y)>3500) continue;
    int ix = (x+4001)/100;
    int iy = (y+3501)/100;
    int iz = (z+ 501)/100;
    if(ix<0||ix>80||iy<0||iy>70) continue;
    if(iz>120) break;
    field[iz].Bx[ix][iy] = bx;
    field[iz].By[ix][iy] = by;
    field[iz].Bz[ix][iy] = bz;
    npt++;
  }
  fclose(map);
}
/*-------------------------------------------*/
void B_xyz(double x, double y, double z, double& B_x, double& B_y, double& B_z) {
  if(fabs(x)>3999||fabs(y)>3499||z<-499||z>=11399) {
    B_x = B_y = B_z = 0;
    return;
  }
  int ix,iy,iz;
  ix = (x+4000)/100;
  iy = (y+3500)/100;
  iz = (z+ 500)/100;
  if(iz>119) iz = 119;
  double dx = (x+4000)/100-ix; double ex = 1-dx;
  double dy = (y+3500)/100-iy; double ey = 1-dy;
  double dz = (z+ 500)/100-iz; double ez = 1-dz;
  if(dx<0||dx>1) printf("!!! x,y,ix,iy,z,dx %f %f  %d %d  %f  %f\n",x,y,ix,iy,z,dx);
  if(dy<0||dy>1) printf("!!! x,y,ix,iy,z,dy %f %f  %d %d  %f  %f\n",x,y,ix,iy,z,dy);
  if(dz<0||dz>1) printf("!!! x,y,ix,iy,z,dz %f %f  %d %d  %f  %f\n",x,y,ix,iy,z,dz);
  B_x = dx*(dy*(dz*field[iz+1].Bx[ix+1][iy+1]+ez*field[iz].Bx[ix+1][iy+1])+ey*(dz*field[iz+1].Bx[ix+1][iy  ]+ez*field[iz].Bx[ix+1][iy]))+
        ex*(dy*(dz*field[iz+1].Bx[ix  ][iy+1]+ez*field[iz].Bx[ix  ][iy+1])+ey*(dz*field[iz+1].Bx[ix  ][iy  ]+ez*field[iz].Bx[ix  ][iy]));
  B_y = dx*(dy*(dz*field[iz+1].By[ix+1][iy+1]+ez*field[iz].By[ix+1][iy+1])+ey*(dz*field[iz+1].By[ix+1][iy  ]+ez*field[iz].By[ix+1][iy]))+
        ex*(dy*(dz*field[iz+1].By[ix  ][iy+1]+ez*field[iz].By[ix  ][iy+1])+ey*(dz*field[iz+1].By[ix  ][iy  ]+ez*field[iz].By[ix  ][iy]));
  B_z = dx*(dy*(dz*field[iz+1].Bz[ix+1][iy+1]+ez*field[iz].Bz[ix+1][iy+1])+ey*(dz*field[iz+1].Bz[ix+1][iy  ]+ez*field[iz].Bz[ix+1][iy]))+
        ex*(dy*(dz*field[iz+1].Bz[ix  ][iy+1]+ez*field[iz].Bz[ix  ][iy+1])+ey*(dz*field[iz+1].Bz[ix  ][iy  ]+ez*field[iz].Bz[ix  ][iy]));
}
/*
void B_xyz(double x, double y, double z, double& B_x, double& B_y, double& B_z) {
    double dx,dy,dz,dist2,dist,ux,uy,uz,mu;
    double MY_DIPOLE = -1.6e10; 
    dx = x;
    dy = y-4000;
    dz = z-5000;
    dist2 = dx*dx+dy*dy+dz*dz; dist = sqrt(dist2);
    ux = dx/dist; uy = dy/dist; uz = dz/dist;
    mu = MY_DIPOLE*uy;
    B_x = 3*mu*ux/dist/dist2;
    B_y = (3*mu*uy-MY_DIPOLE)/dist/dist2;
    B_z = 3*mu*uz/dist/dist2;

    dy = y+4000;
    dist2 = dx*dx+dy*dy+dz*dz; dist = sqrt(dist2);
    ux = dx/dist; uy = dy/dist; uz = dz/dist;
    mu = MY_DIPOLE*uy;
    B_x += 3*mu*ux/dist/dist2;
    B_y += (3*mu*uy-MY_DIPOLE)/dist/dist2;
    B_z += 3*mu*uz/dist/dist2;
    return;                          
}
*/

void f(double z, double dz, double x, double y,double tx, double ty, double qop, double &dx, double &dy, double& dtx, double& dty)
// equation differentielle de propagation
{
  double B_x, B_y, B_z; 
  dx = tx*dz; dy = ty*dz;
  B_xyz(x,y,z,B_x,B_y,B_z);
  double fact = .299792458*qop*fabs(dz)*sqrt(1+tx*tx+ty*ty);
  dtx = fact*(tx*ty*B_x-(1+tx*tx)*B_y+ty*B_z);
  dty = fact*((1+ty*ty)*B_x-tx*ty*B_y-tx*B_z);
}
/*--------------------------------------------------------------------------*/
void fx(double x, double dx, double y, double z,double uy, double uz, double qop, double &dy, double &dz, double& duy, double& duz)
// equation differentielle de propagation
{
  double B_x, B_y, B_z; 
  dy = uy*dx; dz = uz*dx;
  B_xyz(x,y,z,B_x,B_y,B_z);
  double fact = .299792458*qop*fabs(dx)*sqrt(1+uy*uy+uz*uz);
  duy = fact*(uy*uz*B_y-(1+uy*uy)*B_z+uz*B_x);
  duz = fact*((1+uz*uz)*B_y-uy*uz*B_z-uy*B_x);
}
/*--------------------------------------------------------------------------*/
void step_RK4(double &z, double dz, double& x, double& y, double& tx, double& ty, double qop)
// step de Runge-Kutta d'ordre 4
{
  //if(TEST_PRINT) printf("z %7.2f %7.2f %7.2f %6.2f  %6.3f %6.3f\n",x,y,z,dz,tx,ty);
  double d1x,d1y,d1tx,d1ty,d2x,d2y,d2tx,d2ty,d3x,d3y,d3tx,d3ty,d4x,d4y,d4tx,d4ty;
  f(z     ,dz,  x,      y,      tx,       ty,       qop,  d1x,d1y,d1tx,d1ty);
  f(z+dz/2,dz,  x+d1x/2,y+d1y/2,tx+d1tx/2,ty+d1ty/2,qop,  d2x,d2y,d2tx,d2ty);
  f(z+dz/2,dz,  x+d2x/2,y+d2y/2,tx+d2tx/2,ty+d2ty/2,qop,  d3x,d3y,d3tx,d3ty);
  f(z+dz  ,dz,  x+d3x  ,y+d3y  ,tx+d3tx  ,ty+d3ty  ,qop,  d4x,d4y,d4tx,d4ty);
  x  += (d1x+ 2*d2x+ 2*d3x+ d4x)/6;
  y  += (d1y+ 2*d2y+ 2*d3y+ d4y)/6;
  tx += (d1tx+2*d2tx+2*d3tx+d4tx)/6;
  ty += (d1ty+2*d2ty+2*d3ty+d4ty)/6;
  z += dz;
}
/*--------------------------------------------------------------------------*/
void stepx_RK4(double &x, double dx, double& y, double& z, double& uy, double& uz, double qop)
// step de Runge-Kutta d'ordre 4
{
  //if(TEST_PRINT) printf("x %7.2f %7.2f %7.2f %6.2f  %6.3f %6.3f\n",x,y,z,dx,uy,uz);
  double d1y,d1z,d1uy,d1uz,d2y,d2z,d2uy,d2uz,d3y,d3z,d3uy,d3uz,d4y,d4z,d4uy,d4uz;
  fx(x     ,dx,  y,      z,      uy,       uz,       qop,  d1y,d1z,d1uy,d1uz);
  fx(x+dx/2,dx,  y+d1y/2,z+d1z/2,uy+d1uy/2,uz+d1uz/2,qop,  d2y,d2z,d2uy,d2uz);
  fx(x+dx/2,dx,  y+d2y/2,z+d2z/2,uy+d2uy/2,uz+d2uz/2,qop,  d3y,d3z,d3uy,d3uz);
  fx(x+dx  ,dx,  y+d3y  ,z+d3z  ,uy+d3uy  ,uz+d3uz  ,qop,  d4y,d4z,d4uy,d4uz);
  y  += (d1y+ 2*d2y+ 2*d3y+ d4y)/6;
  z  += (d1z+ 2*d2z+ 2*d3z+ d4z)/6;
  uy += (d1uy+2*d2uy+2*d3uy+d4uy)/6;
  uz += (d1uz+2*d2uz+2*d3uz+d4uz)/6;
  x += dx;
}
/*--------------------------------------------------------------------------*/
int nstep_RK4(int n, double z, double dz, double xi, double yi, double txi, double tyi, double qopi, double eloss,
               double& xf, double& yf, double& txf, double& tyf, double& qopf)
{
  xf = xi; yf = yi; txf = txi; tyf = tyi; qopf = qopi;
  for(int i=0; i<n; i++) {
    step_RK4(z,dz,xf,yf,txf,tyf,qopf);
    //qopf *= 1+eloss*fabs(dz*sqrt(1+txf*txf+tyf*tyf)*qopf);
  }
  if(fabs(z-5500)<1500&&(fabs(xf/z)>.3||fabs(yf/z)>.25)) return 0;
  if(fabs(xf)> 3000 || fabs(yf)> 2700) return 0;
  
  return 1;
}




int RK4_random(){
TRandom3 rnd(2);

double limit_tx=0.6,limit_ty=0.5, qoplimit=1/2000., xVelo=0., yVelo=0.,eloss=0., zVelo=0., zSci=8520.;
double initial_txO=-limit_tx/2, initial_tyO=-limit_ty/2, initial_qop=1/120000.;
double txO, tyO, qop, xSci, ySci, txSci, tySci, qopf,p,pt,PT2,eta,phi, MAGNET;
int ntx=31, nty=26, nqop=20, nstep=50, track=0;

ReadField("field_v5r0.map"); 
TFile f("field_toyTracks_random.root", "recreate");
TTree* t = new TTree("t","t");

t->Branch("xVelo",&xVelo,"xVelo/D");
t->Branch("yVelo",&yVelo,"yVelo/D");
t->Branch("zVelo",&zVelo,"zVelo/D");
t->Branch("txO",&txO,"txO/D");
t->Branch("tyO",&tyO,"tyO/D");
t->Branch("qop",&qop,"qop/D");
t->Branch("p",&p,"p/D");
t->Branch("pt",&pt,"pt/D");
t->Branch("PT2",&PT2,"PT2/D");
t->Branch("eta",&eta,"eta/D");
t->Branch("phi",&phi,"phi/D");
t->Branch("eloss",&eloss,"eloss/D");
t->Branch("xSci",&xSci,"xSci/D");
t->Branch("ySci",&ySci,"ySci/D");
t->Branch("zSci",&zSci,"zSci/D");
t->Branch("txSci",&txSci,"txSci/D");
t->Branch("tySci",&tySci,"tySci/D");
t->Branch("qopf",&qopf,"qopf/D");
t->Branch("nstep",&nstep,"nstep/I");
t->Branch("MAGNET",&MAGNET,"MAGNET/D");
/*
for(int i=0; i<ntx; i++){
  txO = initial_txO + limit_tx/(ntx-1)*i;  
  for(int j=0; j<nty; j++){
    tyO = initial_tyO + limit_ty/(nty-1)*j;
    for(int k=0; k<nqop; k++){
      qop = initial_qop + (qoplimit-initial_qop)/(nqop-1) * k;
      p=1/qop;
      PT2 = p*p*(txO*txO+tyO*tyO)/(1+txO*txO+tyO*tyO);
      eta=0;
      if(PT2>0) {eta = acosh(p/sqrt(PT2));}
      phi = atan2(tyO,txO);
      pt=sqrt(PT2);
      MAGNET=-1;
      if(nstep_RK4(nstep,zVelo,(zSci-zVelo)/nstep,xVelo,yVelo,txO,tyO,qop,eloss,xSci, ySci, txSci, tySci, qopf))
      t->Fill();

      qop = -qop;
      p=-1/qop;
      PT2 = p*p*(txO*txO+tyO*tyO)/(1+txO*txO+tyO*tyO);
      eta=0;
      if(PT2>0) {eta = acosh(p/sqrt(PT2));}
      phi = atan2(tyO,txO);
      pt=sqrt(PT2);
      MAGNET=-1;
      if(nstep_RK4(nstep,zVelo,(zSci-zVelo)/nstep,xVelo,yVelo,txO,tyO,qop,eloss,xSci, ySci, txSci, tySci, qopf))
      t->Fill();
      track++;
      cout<<"processing "<<track*2<<" / "<<ntx*nty*nqop*2<<endl;
    }
  }
}*/

for(int i=0; i<10000; i++){
if(rnd.Rndm()>0.5){
qop = rnd.Rndm() * qoplimit;
}else{
qop = -1 * rnd.Rndm() * qoplimit;}
if(rnd.Rndm()>0.5){
txO = rnd.Rndm() * initial_txO;
}else{
txO = -1 * rnd.Rndm() * initial_txO;}
if(rnd.Rndm()>0.5){
tyO = rnd.Rndm() * initial_tyO;
}else{
tyO = -1 * rnd.Rndm() * initial_tyO;}


      p=1./fabs(qop);
      PT2 = p*p*(txO*txO+tyO*tyO)/(1+txO*txO+tyO*tyO);
      eta=0;
      if(PT2>0) {eta = acosh(p/sqrt(PT2));}
      phi = atan2(tyO,txO);
      pt=sqrt(PT2);
      MAGNET=-1;
      if(nstep_RK4(nstep,zVelo,(zSci-zVelo)/nstep,xVelo,yVelo,txO,tyO,qop,eloss,xSci, ySci, txSci, tySci, qopf))
      t->Fill();
}
t->Write();
f.Close();

return 1;
}
