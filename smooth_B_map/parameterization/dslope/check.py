import ROOT as r

from LHCbStyle import *
LHCbStyle("LHCbStyleZ", makeNew=True,colz=True)
# r.gROOT.SetBatch(True)
r.gStyle.SetOptStat(0)
r.gStyle.SetTextFont(132)
r.gStyle.SetTitleFont(132, "title")
r.gROOT.ForceStyle()
r.gStyle.SetTitleAlign(33)
r.gStyle.SetTitleX(.50)
r.gStyle.SetCanvasPreferGL(1)
ff = r.TFile("data_smooth/smooth_toyTracks_ty_0.2_cut_plots.root","READ")
cc = r.TCanvas("c1","c1",1500,800)
cc.Divide(3,2)
histos     = [ ff.Get("c{}_par_fitted".format(i)) for i in range(0,5)]
histos_res = [ ff.Get("c{}_par_fitted_residual".format(i)) for i in range(0,5)]
for i,h in enumerate(histos):
    cc.cd(i+1)
    histos[i].Draw("lego")
    histos[i].GetXaxis().CenterTitle()
    histos[i].GetYaxis().CenterTitle()
    histos[i].GetXaxis().SetTitleOffset(1.4)
    histos[i].GetYaxis().SetTitleOffset(1.8)
    histos[i].GetZaxis().SetTitleOffset(1.2)

    r.gPad.SetGridx()
    r.gPad.SetGridy()
cc.Draw()
cc.SetGrid()
cc.SaveAs("Grid_FitShapes.pdf")
cc2 = r.TCanvas("c2","c2",1500,800)
cc2.Divide(3,2)
for i,h in enumerate(histos_res):
    cc2.cd(i+1)
    histos_res[i].Draw("colz")
    histos_res[i].GetXaxis().CenterTitle()
    histos_res[i].GetYaxis().CenterTitle()
    histos_res[i].GetXaxis().SetTitleOffset(1.4)
    histos_res[i].GetYaxis().SetTitleOffset(1.8)
    histos_res[i].GetZaxis().SetTitleOffset(1.2)
    histos_res[i].GetZaxis().SetRangeUser(-100,100)
    r.gPad.SetGridx()
    r.gPad.SetGridy()
cc2.Draw()
cc2.SetGrid()
cc2.SaveAs("residual.pdf")

